import React, { Component } from 'react';
import { DrawerNavigator,NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  StyleSheet,
  View, 
  Image,
  TouchableNativeFeedback,
  StatusBar,
  Text,
  ScrollView
} from 'react-native';
import { 
  Button,
  SocialIcon,
  Input
} from 'react-native-elements';
import { color } from 'react-native-material-design-styles';

// Services
import Auth from '../../services/Auth';

const AuthInstance = new Auth();

export default class SignInScreen extends Component {
  constructor(props){
    super(props);
    this.state = {};
  }
  OnSubmit() {
    console.log(this.state)
  //   if(this.state) {
  //     if(this.state.errorEmail===undefined && this.state.errorPassword===undefined)
  //       console.log(this.state)
  //       this.props.navigation('Splash') 
  //       // AuthInstance.SignIn(this.state)
  //         // .then(success => {
  //         //   if(success){
  //         //   }
  //         // })
  //   }else {

  //   }
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView style={styles.scrollView}>
        <StatusBar
          backgroundColor={color.paperOrange300.color}
          barStyle="light-content"
          hidden={true}
        />
        <View style={styles.container}>
          <Image
            resizeMode={'center'}
            style={{width: 280, height:280}}
            source={require("../../resource/img/logorojo.png")}
          />
          <View style={styles.content}>
            <Input
              placeholderTextColor='#FFF'
              keyboardType={'email-address'}
              containerStyle={styles.containerStyle}
              placeholder="Correo Electronico"
              inputStyle={styles.inputStyle}
              color="#FFF"
              shake
              leftIcon={
                <Icon
                  name='email'
                  size={16}
                  color='white'
                />
              }
              onChangeText={(text) => this.setState({ email: text })}
            />
            <Input
              placeholderTextColor='#FFF'
              placeholder="Contraseña"
              containerStyle={styles.containerStyle}
              inputStyle={styles.inputStyle}
              color="#FFF"
              shake
              segurity={true}
              leftIcon={
                <Icon
                  name='lock'
                  size={16}
                  color='white'
                />
              }
              onChangeText={(text) => this.setState({ password: text })}
            />
            <View style={styles.contentBtn}>
              <TouchableNativeFeedback onPress={() => this.handlerSubmit()}>
                <View style={styles.btn}>
                  <Text style={styles.btn_text}>Entrar</Text>
                </View>
              </TouchableNativeFeedback>
              <TouchableNativeFeedback  onPress={() => navigate('SignUp')}>
                <View style={styles.btn}>
                  <Text style={styles.btn_text}>Registrate</Text>
                </View>
              </TouchableNativeFeedback>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: '#17afbd',
  },
  container: {
    flex: 1,
    backgroundColor: '#17afbd',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    width: 250,
  },
  title: {
    fontSize: 20,
    color: 'white',
  }, 
  containerStyle: {
    width: 'auto',
    height: 52,
    marginLeft: 0,
    marginBottom: 6,
    borderRadius: 5,
    backgroundColor: '#99e5f3',
    borderBottomWidth: 0,
    borderBottomColor: 'transparent',
  },
  inputStyle: {
    flex: 1,
    color: 'white',
    padding: 5,
  },
  contentBtn: { 
    marginTop: 10,
  },
  btn: {
    flex: 1,
    position: 'relative',
    width: 250,
    height: 42,
    margin: 0,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
    padding: 5,
  },
  btn_text: {
    color: '#e52427',
  }
})
