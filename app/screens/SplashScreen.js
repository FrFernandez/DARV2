import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { DrawerNavigator } from 'react-navigation';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Alert,
  TouchableNativeFeedback,
  ActivityIndicator,
  StatusBar
} from 'react-native';
import { color } from 'react-native-material-design-styles';

// Services
import Auth from '../services/Auth';

const AuthInstance = new Auth();

export default class SplashScreen extends Component {
  constructor(props){
    super(props);
    AuthInstance.isLogged(success=>{
      if(success)
        this.resetTo('Authorized'); 
      else
        this.resetTo('Unauthorized');
    })
  }
  render() {
    return (
      <View style={styles.container}>
        <StatusBar
            backgroundColor={color.paperOrange300.color}
            barStyle="light-content"
            hidden={true}
          />
        <View style={{flexDirection: 'row', flex: 1}}>
					<View style={styles.leftBar}></View>
					<View style={styles.rightBar}></View>
				</View>
        <View style={styles.content}>
          <Image style={styles.Image} source={require('../resource/img/logo.png')}/>
          <ActivityIndicator size="large" color="#FFF" />
        </View>
      </View>
    );
  }
  resetTo(route) {
    const actionToDispatch = NavigationActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: route })],
    });
    this.props.navigation.dispatch(actionToDispatch)
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#ffffff',
  },
  content: {
    flex: 1,
    position: 'absolute',
    justifyContent: 'center',
  },
  Image: {
    resizeMode: 'stretch',
    width: 280,
    height: 300,
    zIndex: 2,
    marginBottom: 30
  },
  leftBar: {
		flex: 1, 
		backgroundColor:"#E52427"
	},
	rightBar: {
		flex: 1,  
		backgroundColor:"#17AFBD"
	},
});
