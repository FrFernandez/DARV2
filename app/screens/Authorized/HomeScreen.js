import React, { Component } from 'react';
import { DrawerNavigator } from 'react-navigation';

import {
  StyleSheet,
  View,
  Text,
  Image,
  Alert,
  StatusBar,
  TouchableNativeFeedback,
} from 'react-native';
import { color } from 'react-native-material-design-styles';

export default class HomeScreen extends Component {
  static navigationOptions = {
    drawerLabel: 'Inicio',
    title: 'Inicio',
  };
  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={color.paperOrange300.color}
          barStyle="light-content"
          hidden={true}
        />
        <Text style={styles.title}>AprecioDePana Movil</Text>
      </View>
    );
  }
  onLogin() {
    console.log('init')
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    position: 'relative',
    backgroundColor: '#fff',
  },
  title: {
    textAlign: 'center',
    fontSize: 26,
    color: '#000',
    marginTop: 20,
    marginBottom: 30,
  }
});
