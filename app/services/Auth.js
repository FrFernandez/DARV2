import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';

import { NavigationActions } from 'react-navigation';

export default class Auth extends Component {
  constructor(props){
    super(props);
    this.tokens = {};
    this.data = {};
  }
  GetToken() {
    return new Promise((resolve,reject) => {
      AsyncStorage.getItem('@app:sessionkey',
        (err,result) => {
          if(!err){
            if(result!=null){
              let tokens = result.split(',');
              if(tokens[0]!=undefined && tokens[1]!=undefined){
                this.setState({
                  tokens: {
                    token: tokens[0],
                    tokenSecret: tokens[1] 
                  }
                })
                console.log('GetTokens Success');
                resolve(this.tokens);
              }else {
                reject();
              }
            }else {
              console.log('Not found Token')
              reject()
            }
          }else {
            console.log(err)
            reject(err)
          }
        }
      )
    })
  }
  GetUser(){
    fetch('http://50.21.179.35:8082/users?token='+this.tokens.token+'&tokensecret='+this.tokens.tokenSecret)
    .then(response => response.json())
    .then(responseJson => {
      console.log(responseJson)
      this.setState({data: responseJson})
    })
  }
  isLogged(cb) {
    this.GetToken()
      .then(success=>{
        this.GetUser()
        return cb ? cb(true): false
      })
      .catch(error=>{
        return cb ? cb(false): false
      })
  }
  SignIn(data){
    return new Promise((resolve,reject) => {
      fetch('http://50.21.179.35:8082/auth/login', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.token && responseJson.tokenSecret){
          AsyncStorage.setItem('@app:sessionkey',responseJson.token+','+responseJson.tokenSecret,
            err => {
              console.log(err)
              if(!err){
                console.log('Success SignIn Congratulations')
                this.isLogged();
                resolve(true);
              }else {
                console.log(err)
                reject(err);
              }
            }
          )
        }
      })
    })

  }
  SignOut() {
    return new Promise((resolve,reject) => {
      fetch('http://50.21.179.35:8082/logout/'+this.tokens.token+'/'+this.tokens.tokenSecret)
      .then(response => response.json())
      .then(responseJson => {
        AsyncStorage.removeItem('@app:sessionkey',
          error => {
            if(!error){
              delete this.tokens;
              delete this.data;
              this.setState(this.tokens);
              this.setState(this.data);
              resolve(true);
            }else {
              delete this.tokens;
              delete this.data;
              this.setState(this.tokens);
              this.setState(this.data);
              reject(false);
              console.log(error);
            }
          }
        )
      })
    })
  }
  HandlerToastError(type,error){
    switch(type){
      case '0':
        break;
      case '1':
        break;
      case '2':
        break;
    }
    console.log(error)
  }
}